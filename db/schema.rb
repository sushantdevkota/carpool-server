# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151016130735) do

  create_table "approval_offer_requests", force: :cascade do |t|
    t.integer  "offer_id"
    t.integer  "request_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "approval_state"
    t.integer  "approval_message"
  end

  add_index "approval_offer_requests", ["offer_id"], name: "index_approval_offer_requests_on_offer_id"
  add_index "approval_offer_requests", ["request_id"], name: "index_approval_offer_requests_on_request_id"

  create_table "comments", force: :cascade do |t|
    t.integer  "story_id"
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["story_id"], name: "index_comments_on_story_id"

  create_table "gcms", force: :cascade do |t|
    t.string   "user_id",    null: false
    t.string   "gcm_id",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "gcms", ["gcm_id"], name: "index_gcms_on_gcm_id", unique: true

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "story_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "likes", ["story_id"], name: "index_likes_on_story_id"
  add_index "likes", ["user_id"], name: "index_likes_on_user_id"

  create_table "offers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ride_id"
    t.integer  "vehicle_type"
    t.string   "vehicle_no"
    t.integer  "no_of_seats"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "occupied_seats"
  end

  add_index "offers", ["ride_id"], name: "index_offers_on_ride_id"
  add_index "offers", ["user_id"], name: "index_offers_on_user_id"

  create_table "push_notification_tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "push_notification_token"
    t.integer  "device_type"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "push_notification_tokens", ["user_id"], name: "index_push_notification_tokens_on_user_id"

  create_table "rails_push_notifications_apns_apps", force: :cascade do |t|
    t.text     "apns_dev_cert"
    t.text     "apns_prod_cert"
    t.boolean  "sandbox_mode"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "rails_push_notifications_gcm_apps", force: :cascade do |t|
    t.string   "gcm_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_mpns_apps", force: :cascade do |t|
    t.text     "cert"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rails_push_notifications_notifications", force: :cascade do |t|
    t.text     "destinations"
    t.integer  "app_id"
    t.string   "app_type"
    t.text     "data"
    t.text     "results"
    t.integer  "success"
    t.integer  "failed"
    t.boolean  "sent",         default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "rails_push_notifications_notifications", ["app_id", "app_type", "sent"], name: "app_and_sent_index_on_rails_push_notifications"

  create_table "requests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ride_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "message"
  end

  add_index "requests", ["ride_id"], name: "index_requests_on_ride_id"
  add_index "requests", ["user_id"], name: "index_requests_on_user_id"

  create_table "rides", force: :cascade do |t|
    t.float    "lat_from"
    t.float    "lat_to"
    t.float    "long_from"
    t.float    "long_to"
    t.datetime "time_from"
    t.datetime "time_to"
    t.integer  "allowed_gender"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "from_string"
    t.string   "to_string"
  end

  create_table "stories", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "likes"
  end

  add_index "stories", ["user_id"], name: "index_stories_on_user_id"

  create_table "temp_offer_requests", force: :cascade do |t|
    t.integer  "offer_id"
    t.integer  "user_id"
    t.boolean  "is_accepted"
    t.string   "approval_message"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "request_message"
  end

  add_index "temp_offer_requests", ["offer_id"], name: "index_temp_offer_requests_on_offer_id"
  add_index "temp_offer_requests", ["user_id"], name: "index_temp_offer_requests_on_user_id"

  create_table "temp_request_offers", force: :cascade do |t|
    t.integer  "request_id"
    t.integer  "user_id"
    t.boolean  "is_accepted"
    t.string   "approval_message"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "offer_message"
  end

  add_index "temp_request_offers", ["request_id"], name: "index_temp_request_offers_on_request_id"
  add_index "temp_request_offers", ["user_id"], name: "index_temp_request_offers_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "facebook_id", null: false
    t.float    "latest_lat"
    t.float    "latest_long"
    t.string   "image_url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "gender"
  end

  add_index "users", ["facebook_id"], name: "index_users_on_facebook_id", unique: true

end
