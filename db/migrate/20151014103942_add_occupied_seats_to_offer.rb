class AddOccupiedSeatsToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :occupied_seats, :integer
  end
end
