class CreateApprovalOfferRequests < ActiveRecord::Migration
  def change
    create_table :approval_offer_requests do |t|
      t.references :offer, index: true, foreign_key: true
      t.references :request, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
