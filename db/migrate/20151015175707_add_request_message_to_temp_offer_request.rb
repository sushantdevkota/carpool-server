class AddRequestMessageToTempOfferRequest < ActiveRecord::Migration
  def change
    add_column :temp_offer_requests, :request_message, :string
  end
end
