class AddToStringToRide < ActiveRecord::Migration
  def change
    add_column :rides, :to_string, :string
  end
end
