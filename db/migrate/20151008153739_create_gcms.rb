class CreateGcms < ActiveRecord::Migration
  def change
    create_table :gcms do |t|
      t.integer :user_id
      t.string :gcm_id

      t.timestamps null: false
    end
    add_index :gcms, :gcm_id, unique: true
    change_column :gcms, :user_id, :string, null: false
    change_column :gcms, :gcm_id, :string, null: false
  end
end
