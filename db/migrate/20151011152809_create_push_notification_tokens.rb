class CreatePushNotificationTokens < ActiveRecord::Migration
  def change
    create_table :push_notification_tokens do |t|
      t.references :user, index: true, foreign_key: true
      t.string :push_notification_token
      t.integer :device_type

      t.timestamps null: false
    end
  end
end
