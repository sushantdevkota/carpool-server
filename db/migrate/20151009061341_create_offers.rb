class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :ride, index: true, foreign_key: true
      t.integer :vehicle_type
      t.string :vehicle_no
      t.integer :no_of_seats

      t.timestamps null: false
    end
  end
end
