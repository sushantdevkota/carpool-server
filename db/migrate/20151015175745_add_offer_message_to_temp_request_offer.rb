class AddOfferMessageToTempRequestOffer < ActiveRecord::Migration
  def change
    add_column :temp_request_offers, :offer_message, :string
  end
end
