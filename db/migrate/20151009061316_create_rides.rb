class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.float :lat_from
      t.float :lat_to
      t.float :long_from
      t.float :long_to
      t.datetime :time_from
      t.datetime :time_to
      t.integer :allowed_gender

      t.timestamps null: false
    end
  end
end
