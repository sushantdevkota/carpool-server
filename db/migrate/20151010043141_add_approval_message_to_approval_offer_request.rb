class AddApprovalMessageToApprovalOfferRequest < ActiveRecord::Migration
  def change
    add_column :approval_offer_requests, :approval_message, :integer
  end
end
