class CreateTempOfferRequests < ActiveRecord::Migration
  def change
    create_table :temp_offer_requests do |t|
      t.references :offer, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :is_accepted
      t.string :approval_message

      t.timestamps null: false
    end
  end
end
