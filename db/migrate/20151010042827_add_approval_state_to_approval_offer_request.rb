class AddApprovalStateToApprovalOfferRequest < ActiveRecord::Migration
  def change
    add_column :approval_offer_requests, :approval_state, :integer
  end
end
