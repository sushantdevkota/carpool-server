class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :facebook_id
      t.float :latest_lat
      t.float :latest_long
      t.string :image_url

      t.timestamps null: false
    end
    add_index :users, :facebook_id, unique: true
    change_column :users, :name, :string, null: false
    change_column :users, :facebook_id, :string, null: false
  end
end
