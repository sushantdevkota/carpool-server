Time::DATE_FORMATS[:default] = "%Y-%m-%d %H:%M"


class ActiveSupport::TimeWithZone
  def as_json(options = {})
    to_i
  end
end