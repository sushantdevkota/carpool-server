module ApplicationHelper
  def get_response_hash codeInt, messageString
    {code: codeInt, message: "#{messageString}"}
  end
end
