module NotificationHelper
  def create_ios_app
    app = RailsPushNotifications::APNSApp.new
    app.apns_dev_cert = File.read('path/to/your/development/certificate.pem')
    app.apns_prod_cert = File.read('path/to/your/production/certificate.pem')
    app.sandbox_mode = true
    app.save
  end

  def create_android_app
    app = RailsPushNotifications::GCMApp.new
    app.gcm_key = 'AIzaSyD661SAcJlWzj74Y3Vlv3I4g4M5bsdyWM0'
    return app
  end

  def send_ios_notification(destinations, params)
    app = create_ios_app
    aps_data = {alert: params[:message], sound: 'true', badge: 1}
    params.delete(:message)
    params[:aps] = aps_data
    app.notifications.create(
        destinations: destinations,
        data: params
    )
    app.push_notifications
  end

  def send_android_notification(destinations, message)
    # app = create_android_app
    #
    # if app.save
    #   notif = app.notifications.build(
    #       destinations: destinations,
    #       data: {text: message}
    #   )
    #
    #   if notif.save
    #     app.push_notifications
    #     notif.reload
    #     puts "Notification successfully pushed through!. Results #{notif.results.success} succeded, #{notif.results.failed} failed"
    #   end
    # end

    gcm_key = "AIzaSyDGy7G3QIt7-tCz1CuHslkB3jCzTz_3-9Q"

    require 'gcm'

    gcm = GCM.new(gcm_key)
    # you can set option parameters in here
    #  - all options are pass to HTTParty method arguments
    #  - ref: https://github.com/jnunemaker/httparty/blob/master/lib/httparty.rb#L40-L68
    #  gcm = GCM.new("my_api_key", timeout: 3)

    # registration_ids= ["12", "13"] # an array of one or more client registration IDs

    options = {data: message, collapse_key: "updated_score"}
    response = gcm.send(destinations, options)

    puts "Notification log" + response.to_param

  end

  def request_for_an_offer(offer_id, user_id)

    offer = Offer.find_by(id: offer_id)
    requested_user = User.find_by(id: user_id)

    user_with_offer = User.find_by(id: offer[:user_id])

    puts "user notification token param  ----------------" +user_with_offer.to_param
    android_notification_tokens = PushNotificationToken.where(user_id: user_with_offer[:id])

    push_notification_token_android_array = []
    push_notification_token_ios_array = []

    android_notification_tokens.each do |push_notification_token|
      puts "push notification token param  ----------------" +push_notification_token.to_param

      if push_notification_token[:device_type] == 0
        push_notification_token_android_array.push(push_notification_token[:push_notification_token])
      elsif push_notification_token[:device_type] == 1
        push_notification_token_ios_array.push(push_notification_token[:push_notification_token])
      end
    end

    message_map = {notification_type: 0, user_info: requested_user, ror_id: offer_id,
                   message: "#{requested_user[:name]} has requested for your offer"}

    send_android_notification push_notification_token_android_array, message_map
  end

  def offer_for_a_request(request_id, user_id)

    # puts "request id ----------------- offer for a request" + request_id

    request = Request.find_by(id: request_id)
    offerred_user = User.find_by(id: user_id)

    user_with_request = User.find_by(id: request[:user_id])

    android_notification_tokens = PushNotificationToken.where(user_id: user_with_request[:id])

    push_notification_token_android_array = []
    push_notification_token_ios_array = []

    android_notification_tokens.each do |push_notification_token|
      if push_notification_token[:device_type] == 0
        push_notification_token_android_array.push(push_notification_token[:push_notification_token])
      elsif push_notification_token[:device_type] == 1
        push_notification_token_ios_array.push(push_notification_token[:push_notification_token])
      end
    end

    message_map = {notification_type: 1, user_info: user_id, ror_id: request_id,
                   message: "#{offerred_user[:name]} has offered for your request"}

    send_android_notification push_notification_token_android_array, message_map
    # send_ios_notification push_notification_token_ios_array, message_map
  end


  def request_accepted_for_an_offer(offer_id, user_id)

    offer = Offer.find_by(id: offer_id)
    accepted_user = User.find_by(id: user_id)

    accepted_user_notification_tokens_objects = PushNotificationToken.where(user_id: user_id).all
    rejected_user_android_tokens = []
    accepted_user_android_tokens = []
    rejected_user_ios_tokens = []
    accepted_user_ios_tokens = []
    rejected_users = []

    requests_for_offer = TempOfferRequest.where(offer_id: offer_id)
    requests_for_offer.each do |request_for_offer|
      unless request_for_offer[:user_id] == user_id
        rejected_users.push(User.find_by(id: request_for_offer[:user_id]))
      end
    end

    rejected_users.each do |user|
      rejected_user_notification_tokens_objects = PushNotificationToken.where(user_id: user[:id])
      rejected_user_notification_tokens_objects.each do |notification_token|
        if notification_token[:device_type] == 0
          rejected_user_android_tokens.push(notification_token[:push_notification_token])
        elsif notification_token[:device_type] == 1
          rejected_user_ios_tokens.push(notification_token[:push_notification_token])
        end

      end
    end

    accepted_user_notification_tokens_objects.each do |notification_token|
      if notification_token[:device_type] == 0
        accepted_user_android_tokens.push(notification_token[:push_notification_token])
      elsif notification_token[:device_type] ==1
        accepted_user_ios_tokens.push(notification_token[:push_notification_token])
      end

    end

    accepted_message_map = {notification_type: 20, user_info: accepted_user, ror_id: offer_id,
                            message: "Your request for an offer has been accepted"}

    rejected_message_map = {notification_type: 21, user_info: accepted_user, ror_id: offer_id,
                            message: "Your request for an offer has been given to other user"}


    send_android_notification accepted_user_android_tokens, accepted_message_map
    # send_ios_notification accepted_user_ios_tokens, accepted_message_map

    send_android_notification rejected_user_android_tokens, rejected_message_map
    # send_ios_notification rejected_user_ios_tokens, rejected_message_map
  end

  def offer_accepted_for_a_request(request_id, user_id)

    request = Request.find_by(id: request_id)
    accepted_user = User.find_by(id: user_id)

    accepted_user_notification_tokens_objects = PushNotificationToken.where(user_id: user_id)
    rejected_user_android_tokens = []
    accepted_user_android_tokens = []
    accepted_user_ios_tokens = []
    rejected_user_ios_tokens = []
    rejected_users = []

    offers_for_request = TempRequestOffer.where(request_id: request_id)

    offers_for_request.each do |offer_for_request|
      unless offer_for_request[:user_id] == user_id
        rejected_users.push(User.find_by(id: offer_for_request[:user_id]))
      end
    end

    rejected_users.each do |user|
      rejected_user_notification_tokens_objects = PushNotificationToken.where(user_id: user[:id])
      rejected_user_notification_tokens_objects.each do |notification_token|
        if notification_token[:device_type] == 0
          rejected_user_android_tokens.push(notification_token[:push_notification_token])
        elsif notification_token[:device_type] == 1
          rejected_user_ios_tokens.push(notification_token[:push_notification_token])
        end
      end
    end

    accepted_user_notification_tokens_objects.each do |notification_token|
      accepted_user_android_tokens.push(notification_token[:push_notification_token])
    end

    accepted_message_map = {notification_type: 30, user_info: accepted_user, ror_id: request_id,
                            message: "Your offer for a request has been accepted"}

    rejected_message_map = {notification_type: 31, user_info: accepted_user, ror_id: request_id,
                            message: "Your offer for an request has been given to other user"}


    send_android_notification accepted_user_android_tokens, accepted_message_map

    send_android_notification rejected_user_android_tokens, rejected_message_map

    # send_ios_notification accepted_user_ios_tokens, accepted_message_map

    # send_ios_notification rejected_user_ios_tokens, rejected_message_map
  end


  def send_offer_created_to_related_request(offer_id)
    offer = Offer.find_by(id: offer_id)
    ride = Ride.find_by(id: offer[:ride_id])

    notifiable_requests = []

    Request.all.each do |request|
      puts "Request is ---------------" + request.to_param
      ride_for_request = Ride.find_by(id: request[:ride_id])
      puts "ride to string is --------------" + ride_for_request[:to_string]
      puts "ride to string is --------------" + ride_for_request[:from_string]
      if ride_for_request[:to_string].include?(ride[:to_string]) && ride_for_request[:from_string].include?(ride[:from_string])
        notifiable_requests.push(request)
      end
    end

    android_notification_tokens = []
    ios_notification_tokens = []

    notifiable_requests.each do |request|
      notifiable_user = User.find_by(id: request[:user_id])
      notification_tokens = PushNotificationToken.where(user_id: notifiable_user[:id])
      notification_tokens.each do |notification_token|
        if notification_token[:device_type] == 0
          android_notification_tokens.push(notification_token[:push_notification_token])
        elsif notification_token[:device_type] == 1
          ios_notification_tokens.push(notification_token[:push_notification_token])
        end
      end
    end

    message_map = {notification_type: 4, ror_id: offer_id,
                   message: "Recommended offer has been created for you request"}
    send_android_notification android_notification_tokens, message_map
    # send_ios_notification ios_notification_tokens, message_map
  end
end
