json.array!(@temp_offer_requests) do |temp_offer_request|
  json.extract! temp_offer_request, :id
  json.url temp_offer_request_url(temp_offer_request, format: :json)
end
