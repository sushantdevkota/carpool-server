json.array!(@temp_request_offers) do |temp_request_offer|
  json.extract! temp_request_offer, :id
  json.url temp_request_offer_url(temp_request_offer, format: :json)
end
