class TempRequestOffersController < ApplicationController
  include UserHelper

  before_action :set_temp_request_offer, only: [:show, :update, :destroy]

  # GET /temp_request_offers
  # GET /temp_request_offers.json
  def index
    @temp_request_offers = TempRequestOffer.all

    render json: @temp_request_offers
  end

  # GET /temp_request_offers/1
  # GET /temp_request_offers/1.json
  def show
    render json: @temp_request_offer
  end

  # POST /temp_request_offers
  # POST /temp_request_offers.json
  def create
    @temp_request_offer = TempRequestOffer.new(temp_request_offer_params)
    db_request_offer = TempRequestOffer.find_by_request_id(@temp_request_offer[:request_id])
    response_hash = nil

    if !db_request_offer.nil? && (db_request_offer[:user_id] == @temp_request_offer[:user_id])
      response_hash = get_response_hash 5, "You have already offered for this request"
      render json: response_hash
      return
    end
    if !db_request_offer.nil? && db_request_offer[:is_accepted]
      response_hash = get_response_hash 5, "This request has already been offered"
    else
      @temp_request_offer[:approval_message] = nil
      @temp_request_offer[:is_accepted] = false
      if @temp_request_offer.save
        response_hash = get_response_hash 0, "Request offer successfully saved"

        offer_for_a_request(@temp_request_offer[:request_id], @temp_request_offer[:user_id])

      else
        response_hash = get_response_hash 5, "Request offer save failed"
      end
    end
    render json: response_hash
  end

  # PATCH/PUT /temp_request_offers/1
  # PATCH/PUT /temp_request_offers/1.json
  def update
    @temp_request_offer = TempRequestOffer.find_by(id: params[:id])
    response_hash = nil

    if @temp_request_offer.update(temp_request_offer_params)
      response_hash = get_response_hash 0, "Request successfully updated"
      if temp_request_offer_params[:is_accepted]
        offer_accepted_for_a_request(@temp_request_offer[:request_id], @temp_request_offer[:user_id])
      end
    else
      response_hash = get_response_hash 5, "Problem updating request state"
    end

    render json: response_hash
  end

  # DELETE /temp_request_offers/1
  # DELETE /temp_request_offers/1.json
  def destroy
    @temp_request_offer.destroy

    head :no_content
  end

  private

  def set_temp_request_offer
    @temp_request_offer = TempRequestOffer.find_by(id: params[:id])
  end

  def temp_request_offer_params
    params.permit(:user_id, :request_id, :is_accepted, :approval_message, :offer_message)
  end
end
