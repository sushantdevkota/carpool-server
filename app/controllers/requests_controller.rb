class RequestsController < ApplicationController
  require 'date'
  before_action :set_request, only: [:show, :update, :destroy]

  # GET /requests
  # GET /requests.json
  def index
    @requests = Request.all
    request_array = []

    requested_by = User.find_by_facebook_id(user_params[:facebook_id])

    @requests.each do |request|

      ride = nil
      unless request.nil?
        ride = Ride.find_by(id: request[:ride_id])

        if requested_by.nil? || ride[:allowed_gender] == 2 || ride[:allowed_gender] == requested_by[:gender]

          ride = ride.attributes
          ride[:message] = request[:message]
          user = User.find_by(id: request[:user_id])
          ride[:user_info] = {}
          unless user.nil?
            ride[:user_info] = user.attributes
          end
          ride[:id] = request[:id]

          approvals = TempRequestOffer.where(request_id: request[:id]).all
          approvals_array = []
          unless approvals.nil?
            approvals.each do |approval|
              approval_attrib = approval.attributes
              approval_attrib[:user_info] = User.find_by(id: approval[:user_id])
              approvals_array.push(approval_attrib)
            end

          end
          ride[:offers] = approvals_array

          # time_diff_in_hours = (Time.now - ride['time_to']) / 1.hour
          #
          # if time_diff_in_hours < 1 && time_diff_in_hours > 0
          #   ride[:expired] = 1
          # else
          #   ride[:expired] = 0
          # end
          #
          # unless time_diff_in_hours > 2
          #   request_array.push(ride)
          # end

          request_array.push(ride)
        end

      end
    end


    render json: request_array
  end

  # GET /requests/1
  # GET /requests/1.json
  def show

    ride = Ride.find_by(id: @request[:ride_id])
    ride[:id] = @request[:id]
    ride = ride.attributes
    ride[:message] = request[:message]
    user = User.find_by(id: @request[:user_id])
    ride[:user_info] = {}
    unless user.nil?
      ride[:user_info] = user.attributes
    end
    render json: ride
  end

  # POST /requests
  # POST /requests.json
  def create
    @request = Request.new(request_params)
    new_ride = Ride.new(ride_params)
    new_ride[:time_from] = DateTime.strptime(ride_params[:time_from], '%s')
    new_ride[:time_to] = DateTime.strptime(ride_params[:time_to], '%s')

    response_hash = nil

    if new_ride.save
      @request[:ride_id] = new_ride[:id]
      if @request.save
        response_hash = get_response_hash 0, "Request successfully saved"

        response_request = new_ride.attributes
        response_request[:message] = @request[:message]
        user = User.find_by(id: @request[:user_id])
        response_request[:user_info] = {}
        unless user.nil?
          response_request[:user_info] = user.attributes
        end

        response_hash[:created_request] = response_request
      else
        Ride.delete(new_ride)
        response_hash = get_response_hash 5, "Problem saving request"
      end
    else
      response_hash = get_response_hash 5, "Problem saving request"
    end
    render json: response_hash
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    @request = Request.find_by(id: params[:id])

    if @request.update(request_params)
      head :no_content
    else
      render json: @request.errors, status: :unprocessable_entity
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    ride = Ride.find_by(id: @request[:ride_id])
    response_hash = nil
    if ride.destroy && @request.destroy
      response_hash = get_response_hash 0, "Request successfully deleted"
    else
      response_hash = get_response_hash 5, "Request susccessfully deleted"
    end

    render json: response_hash
  end

  private

  def set_request
    @request = Request.find_by(id: params[:id])
  end

  def request_params
    params.permit(:user_id, :message)
  end

  def ride_params
    params.permit(:lat_from, :lat_to, :long_from, :long_to, :from_string, :to_string, :time_from, :time_to, :allowed_gender)
  end

  def user_params
    params.permit(:facebook_id)
  end
end
