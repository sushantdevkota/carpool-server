class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = nil

    if comment_params[:story_id].nil?
      @comments = Comment.all
    else
      @comments = Comment.where(story_id: comment_params[:story_id])
    end

    render json: @comments
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    render json: @comment
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)

    response_hash = nil

    if @comment.save
      response_hash = get_response_hash 0, "Comment successfully created"
      response_hash[:comment] = @comment
    else
      response_hash = get_response_hash 5, "Error creating comment"
    end

    render json: response_hash
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    @comment = Comment.find_by(id: params[:id])

    response_hash = nil
    if @comment.update(comment_params)
      response_hash = get_response_hash 0, "Comment successfully updated"
      response_hash[:comment] = @comment
    else
      response_hash = get_response_hash 5, "Error updating comment"
    end

    render json: response_hash
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    response_hash = nil

   if @comment.destroy
     response_hash = get_response_hash 0, "Comment successfully deleted"
   else
     response_hash get_response_hash 5, "Error deleting comment"
   end

    render json: response_hash
  end

  private

  def set_comment
    @comment = Comment.find_by(id: params[:id])
  end

  def comment_params
    params.permit(:content, :story_id)
  end
end
