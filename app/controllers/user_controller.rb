class UserController < ApplicationController
  def new
  end

  def create

    @user = User.new(user_params)
    db_user = User.find_by_facebook_id(@user[:facebook_id])
    response_hash = nil
    new_push_notification = PushNotificationToken.new(push_notification_params)

    if user_params[:facebook_id].nil? || user_params[:facebook_id].to_s.empty?
      response_hash = {code: 5, message: 'Facebook id cannot be empty'}
      render json: response_hash
      return
    end

    if user_params[:name].nil? || user_params[:name].to_s.empty?
      response_hash = {code: 5, message: 'Name cannot be null'}
      render json: response_hash
      return
    end

    if push_notification_params[:push_notification_token].nil? || push_notification_params[:push_notification_token].to_s.empty?
      response_hash = {code: 5, message: 'notification token cannot be null'}
      render json: response_hash
      return
    end

    if push_notification_params[:device_type].nil? || push_notification_params[:device_type].to_s.empty?
      response_hash = {code: 5, message: 'Push notification device type cannot be null'}
      render json: response_hash
      return
    end


    if db_user.nil?
      if @user.valid? && @user.save
        push_notification_params[:user_id] = @user[:id]
        db_push_notification = PushNotificationToken.find_by_push_notification_token(new_push_notification[:push_notification_token])
        if !db_push_notification.nil? && db_push_notification[:device_type] == new_push_notification[:device_type]
          db_push_notification[:user_id] = @user[:id]
          new_push_notification = db_push_notification
        else
          new_push_notification[:user_id] = @user[:id]
        end
        if new_push_notification.valid? &&new_push_notification.save
          response_hash = {code: 0, message: 'User was successfully created, notification token saved successfully'}
        else
          response_hash = {code: 2, message: 'User was successfully created, Error while saving gcm id'}
        end

        response_hash[:user_info] = @user
      else
        response_hash = {code: 5, message: 'Error while registering new user'}
      end
    else
      new_push_notification[:user_id] = db_user[:id]
      prev_notification = PushNotificationToken.find_by_push_notification_token(new_push_notification[:push_notification_token])

      if !prev_notification.nil? && prev_notification[:device_type] == new_push_notification[:device_type]
        prev_notification[:user_id] = db_user[:id]
        if new_push_notification.valid? && prev_notification.save
          response_hash = {code: 1, message: 'Push notification token successfully added to existing user'}
          response_hash[:user_info] = db_user
        else
          response_hash = {code: 4, message: 'Error while adding push notification token on existing user'}
        end
      else
        if new_push_notification.valid? && new_push_notification.save
          response_hash = {code: 1, message: 'Push notification token added successfully added to existing user'}
          response_hash[:user_info] = db_user
        else
          response_hash = {code: 4, message: 'Error while adding push notification on existing user'}
        end
      end

    end
    render json: response_hash
  end

  def update
  end

  def edit
  end

  def destroy
  end

  def index
    render json: User.all
  end

  def show
  end

  private

  def user_params
    params.permit(:name, :facebook_id, :latest_lat, :latest_long, :gender, :image_url)
  end

  def push_notification_params
    params.permit(:push_notification_token, :device_type)
  end
end
