class ApprovalOfferRequestController < ApplicationController
  def create
    @approval_token_request = ApprovalOfferRequest.new(approval_offer_request_params)
    response_hash = nil

    approval = ApprovalOfferRequest.where(request_id: @approval_token_request[:request_id],
                                          offer_id: @approval_token_request[:offer_id],
                                          approval_state: 1)
    
    if approval.length > 0
      response_hash = get_response_hash 5, "Approval already exists"
      render json: response_hash
      return
    end
    if @approval_token_request.save
      response_hash = get_response_hash 0, "Approval Successfully set"
    else
      response_hash = get_response_hash 5, "Error setting approval"
    end

    render json: response_hash
  end

  def destroy
  end

  private

  def approval_offer_request_params
    params.permit(:offer_id, :request_id, :approval_state, :approval_message)
  end
end

