class TempOfferRequestsController < ApplicationController
  before_action :set_temp_offer_request, only: [:show, :update, :destroy]

  # GET /temp_offer_requests
  # GET /temp_offer_requests.json
  def index
    @temp_offer_requests = TempOfferRequest.all

    render json: @temp_offer_requests
  end

  # GET /temp_offer_requests/1
  # GET /temp_offer_requests/1.json
  def show
    render json: @temp_offer_request
  end

  # POST /temp_offer_requests
  # POST /temp_offer_requests.json
  def create
    @temp_offer_request = TempOfferRequest.new(temp_offer_request_params)
    db_offer_request = TempOfferRequest.find_by_offer_id(@temp_offer_request[:offer_id])
    response_hash = nil
    if !db_offer_request.nil? && db_offer_request[:user_id] == @temp_offer_request[:user_id]
      response_hash = get_response_hash 5, "You have already requested for this offer"
      render json: response_hash
      return
    end
    if !db_offer_request.nil? && db_offer_request[:is_accepted]
      response_hash = get_response_hash 5, "This offer has already been accepted"
    else
      @temp_offer_request[:approval_message] = nil
      @temp_offer_request[:is_accepted] = false
      if @temp_offer_request.save
        response_hash = get_response_hash 0, "Offer request successfully saved"

        request_for_an_offer(@temp_offer_request[:offer_id], @temp_offer_request[:user_id])
      else
        response_hash = get_response_hash 5, "Offer request save failed"
      end
    end
    render json: response_hash
  end

  # PATCH/PUT /temp_offer_requests/1
  # PATCH/PUT /temp_offer_requests/1.json
  def update
    @temp_offer_request = TempOfferRequest.find_by(id: params[:id])
    offer = Offer.find(params[:offer_id])
    response_hash = nil

    if @temp_offer_request.update(temp_offer_request_params)

      if temp_offer_request_params[:is_accepted]
        if offer[:no_of_seats] > offer[:occupied_seats]
          offer[:occupied_seats] += 1
          if offer.save
            response_hash = get_response_hash 0, "Offer successfully updated"
            request_accepted_for_an_offer(@temp_offer_request[:offer_id], @temp_offer_request[:user_id])
          else
            response_hash = get_response_hash 5, "Problem saving offer"
          end
        else
          response_hash = get_response_hash 0, "Seat limit reached"
        end

      end
    else
      response_hash = get_response_hash 5, "Problem updating offer state"
    end

    render json: response_hash
  end

  # DELETE /temp_offer_requests/1
  # DELETE /temp_offer_requests/1.json
  def destroy
    @temp_offer_request.destroy

    head :no_content
  end

  private

  def set_temp_offer_request
    @temp_offer_request = TempOfferRequest.find_by(id: params[:id])
  end

  def temp_offer_request_params
    params.permit(:user_id, :offer_id, :is_accepted, :approval_message, :request_message)
  end

end
