class StoriesController < ApplicationController
  before_action :set_story, only: [:show, :update, :destroy]

  # GET /stories
  # GET /stories.json
  def index
    @stories = Story.all

    stories_response = []

    @stories.each do |story|
      story_attr = story.attributes
      like_count = Like.where(story_id: story[:id]).all.count

      story_attr[:like_count] = like_count

      story_attr.delete('likes')

      stories_response.push(story_attr)

    end

    render json: stories_response
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
    render json: @story
  end

  # POST /stories
  # POST /stories.json
  def create
    @story = Story.new(story_params)

    if @story.save
      response_hash = get_response_hash(0, "Story successfully created")
      response_hash[:story] = @story
      render json: response_hash
    else
      render json: @story.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    @story = Story.find_by(id: params[:id])

    if @story.update(story_params)
      response_hash = get_response_hash(0, 'Story successfully updated')
      response_hash[:story] = @story
      render json: response_hash
    else
      render json: @story.errors, status: :unprocessable_entity
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    response_hash = nil
    if @story.destroy
      response_hash = get_response_hash 0, 'Story successfully deleted'
    else
      response_hash = get_response_hash 5, 'Problem while deleting story'
    end

    render json: response_hash
  end

  private

    def set_story
      @story = Story.find_by(id: params[:id])
    end

    def story_params
      params.permit(:user_id, :title, :content)
    end
end
