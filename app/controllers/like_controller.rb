class LikeController < ApplicationController

  def index
    if params[:story_id].nil?
      render json: get_response_hash(5, "Please provide valid story id")
      return
    end

    likes = Like.where(story_id: params[:story_id]).all

    response = {}

    response[:likes_count] = likes.count

    response[:likes_list] = likes

    render json: response
  end

  def create
    user_id = like_params[:user_id]
    story_id = like_params[:story_id]

    like = Like.where(user_id: user_id, story_id: story_id).first

    response_hash = nil

    if like.nil?

      like = Like.new(like_params)

      if like.save
        response_hash = get_response_hash 0, "Story sucessfully liked"
      else
        response_hash = get_response_hash 5, "Problem saving like"
      end
    else
      response_hash = get_response_hash 5, "Story already liked by this user"
    end

    render json: response_hash
  end


  def destroy
    user_id = like_params[:user_id]
    story_id = like_params[:story_id]

    like = Like.where(user_id: user_id, story_id: story_id).first

    response_hash = nil

    if like.nil?
      response_hash = get_response_hash 5, "This story has not been liked by this user"
    else
      if like.destroy
        response_hash = get_response_hash 0, "Story successfully unliked"
      else
        response_hash = get_response_hash 5, "Problem unliking story"
      end

    end

    render json: response_hash
  end


  private

  def like_params
    params.permit(:user_id, :story_id)
  end
end
