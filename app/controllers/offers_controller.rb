class OffersController < ApplicationController
  before_action :set_offer, only: [:show, :update, :destroy]

  # GET /offers
  # GET /offers.json
  def index
    @offers = Offer.all
    ride_array = []

    requested_by = User.find_by_facebook_id(user_params[:facebook_id])

    @offers.each do |offer|

      ride = nil
      unless offer.nil?

        ride = Ride.find_by(id: offer[:ride_id])

        if requested_by.nil? || ride[:allowed_gender] == 2 || ride[:allowed_gender] == requested_by[:gender]

          ride = ride.attributes
          ride[:vehicle_type] = offer[:vehicle_type]
          ride[:vehicle_no] = offer[:vehicle_no]
          ride[:no_of_seats] = offer[:no_of_seats]
          ride[:occupied_seats] = offer[:occupied_seats]
          user = User.find_by(id: offer[:user_id])
          ride[:user_info] = {}
          ride[:id] = offer[:id]
          unless user.nil?
            ride[:user_info] = user.attributes
          end

          approvals = TempOfferRequest.where(offer: offer[:id])
          approvals_array = []
          unless approvals.nil?
            approvals.each do |approval|
              approval_attrib = approval.attributes
              approval_attrib[:user_info] = User.find_by(id: approval[:user_id])
              approvals_array.push(approval_attrib)
            end

          end

          # time_diff_in_hours = (Time.now - ride['time_to']) / 1.hour
          #
          # if time_diff_in_hours < 1 && time_diff_in_hours > 0
          #   ride[:expired] = 1
          # else
          #   ride[:expired] = 0
          # end
          #
          # ride[:requests] = approvals_array
          # unless time_diff_in_hours > 2
          #   ride_array.push(ride)
          # end
          ride[:requests] = approvals_array
          ride_array.push(ride)
        end
      end

    end
    render json: ride_array
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
    ride = Ride.find_by(id: @offer[:ride_id])
    ride_attr = ride.attributes
    ride_attr[:id] = @offer[:id]

    ride_attr[:vehicle_type] = @offer[:vehicle_type]
    ride_attr[:vehicle_no] = @offer[:vehicle_no]
    ride_attr[:no_of_seats] = @offer[:no_of_seats]
    ride_attr[:occupied_seats] = @offer[:occupied_seats]
    user = User.find_by(id: @offer[:user_id])
    ride_attr[:user_info] = {}
    unless user.nil?
      ride_attr[:user_info] = user.attributes
    end
    render json: ride_attr
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(offer_params)
    new_ride = Ride.new(ride_params)
    new_ride[:time_from] = DateTime.strptime(ride_params[:time_from], '%s')
    new_ride[:time_to] = DateTime.strptime(ride_params[:time_to], '%s')
    response_hash = nil

    if new_ride.save
      @offer[:ride_id] = new_ride[:id]
      if @offer.save
        response_hash = get_response_hash 0, "Offer successfully saved"

        response_offer = new_ride.attributes
        response_offer[:vehicle_type] = @offer[:vehicle_type]
        response_offer[:vehicle_no] = @offer[:vehicle_no]
        response_offer[:no_of_seats] = @offer[:no_of_seats]
        response_offer[:occupied_seats] = @offer[:occupied_seats]
        user = User.find_by(id: @offer[:user_id])
        response_offer[:user_info] = {}
        unless user.nil?
          response_offer[:user_info] = user.attributes
        end

        response_hash[:created_offer] = response_offer

        send_offer_created_to_related_request(@offer[:id])
      else
        Ride.delete(new_ride)
        response_hash = get_response_hash 5, "Problem saving offer"
      end
    else
      response_hash = get_response_hash 5, "Problem saving offer"
    end
    render json: response_hash
  end

  # PATCH/PUT /offers/1
  # PATCH/PUT /offers/1.json
  def update
    @offer = Offer.find_by(id: params[:id])

    if @offer.update(offer_params)
      head :no_content
    else
      render json: @offer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    ride = Ride.find_by(id: @offer[:ride_id])
    response_hash = nil
    if ride.destroy && @offer.destroy
      response_hash = get_response_hash 0, "Offer successfully deleted"
    else
      response_hash = get_response_hash 5, "Offer susccessfully deleted"
    end

    render json: response_hash
  end

  private

  def set_offer
    @offer = Offer.find_by(id: params[:id])
  end

  def offer_params
    params.permit(:user_id, :ride_id, :vehicle_type, :vehicle_no, :no_of_seats)
  end

  def ride_params
    params.permit(:lat_from, :lat_to, :long_from, :long_to, :from_string, :to_string, :time_from, :time_to, :allowed_gender)
  end

  def user_params
    params.permit(:facebook_id)
  end
end
