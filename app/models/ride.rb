class Ride < ActiveRecord::Base
  validates :time_from, presence: true
  validates :time_to, presence: true
  validates :to_string, presence: true
  validates :from_string, presence: true
  after_initialize :init

  def init
    self.allowed_gender ||= 3
  end
end
