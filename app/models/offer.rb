class Offer < ActiveRecord::Base
  belongs_to :user
  belongs_to :ride
  after_initialize :init

  def init
    self.occupied_seats ||= 0
  end
end
