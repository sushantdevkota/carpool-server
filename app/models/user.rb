class User < ActiveRecord::Base
  has_many :push_notification_tokens
  has_many :stories
end
