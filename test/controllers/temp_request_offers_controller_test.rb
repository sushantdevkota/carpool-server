require 'test_helper'

class TempRequestOffersControllerTest < ActionController::TestCase
  setup do
    @temp_request_offer = temp_request_offers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:temp_request_offers)
  end

  test "should create temp_request_offer" do
    assert_difference('TempRequestOffer.count') do
      post :create, temp_request_offer: {  }
    end

    assert_response 201
  end

  test "should show temp_request_offer" do
    get :show, id: @temp_request_offer
    assert_response :success
  end

  test "should update temp_request_offer" do
    put :update, id: @temp_request_offer, temp_request_offer: {  }
    assert_response 204
  end

  test "should destroy temp_request_offer" do
    assert_difference('TempRequestOffer.count', -1) do
      delete :destroy, id: @temp_request_offer
    end

    assert_response 204
  end
end
