require 'test_helper'

class TempOfferRequestsControllerTest < ActionController::TestCase
  setup do
    @temp_offer_request = temp_offer_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:temp_offer_requests)
  end

  test "should create temp_offer_request" do
    assert_difference('TempOfferRequest.count') do
      post :create, temp_offer_request: {  }
    end

    assert_response 201
  end

  test "should show temp_offer_request" do
    get :show, id: @temp_offer_request
    assert_response :success
  end

  test "should update temp_offer_request" do
    put :update, id: @temp_offer_request, temp_offer_request: {  }
    assert_response 204
  end

  test "should destroy temp_offer_request" do
    assert_difference('TempOfferRequest.count', -1) do
      delete :destroy, id: @temp_offer_request
    end

    assert_response 204
  end
end
