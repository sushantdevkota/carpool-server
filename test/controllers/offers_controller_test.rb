require 'test_helper'

class OffersControllerTest < ActionController::TestCase
  setup do
    @offer = offers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:offers)
  end

  test "should create offer" do
    assert_difference('Offer.count') do
      post :create, offer: {  }
    end

    assert_response 201
  end

  test "should show offer" do
    get :show, id: @offer
    assert_response :success
  end

  test "should update offer" do
    put :update, id: @offer, offer: {  }
    assert_response 204
  end

  test "should destroy offer" do
    assert_difference('Offer.count', -1) do
      delete :destroy, id: @offer
    end

    assert_response 204
  end
end
